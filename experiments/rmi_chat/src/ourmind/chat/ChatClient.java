
package ourmind.chat;


import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


public interface ChatClient extends Remote {
    
    void login() throws RemoteException;
    void logout() throws RemoteException;
    void receive(Message message) throws RemoteException;

}


