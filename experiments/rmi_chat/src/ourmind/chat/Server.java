
package ourmind.chat;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

public class Server implements ChatServer {

    private Map<String, ChatClient> users = new HashMap<String, ChatClient>();
    
    public Server() {}
    
    @Override
    public boolean login(String userName) {
        boolean result = false;
        try {
            Registry registry = LocateRegistry.getRegistry(Toolkit.Constants.CHAT_SERVER_PORT);
            ChatClient chatClient = (ChatClient) registry.lookup(userName);
            users.put(userName, chatClient);
            System.out.println(userName + " login");
            result = true;
        } catch (Exception e) {
            Toolkit.handleException(e);
            result = false;
        }
        return result;
    }
    
    @Override
    public boolean logout(String userName) {
        boolean result = false;  
        if (users.containsKey(userName)) {
            users.remove(userName);
            System.out.println(userName + " logout");
            result = true;
        } else {
            result = false;
        }
        return result;
    }
    
    @Override
    public void send(Message message) {
        System.out.println( message.getAuthor() + " says: " + message.getContent() );
        
        Iterator<ChatClient> users = this.users.values().iterator();
        while(users.hasNext()) {
            try {
                users.next().receive(message);
            }  catch (Exception e) {
                Toolkit.handleException(e);
            }
        }
    }
    
    public static void main(String[] args) {
        
        try {

            Server obj = new Server();
            ChatServer stub = (ChatServer) UnicastRemoteObject.exportObject(obj, 0);            
            
            Registry registry = LocateRegistry.createRegistry(Toolkit.Constants.CHAT_SERVER_PORT);
            registry.bind(Toolkit.Constants.CHAT_SERVICE_NAME, stub);
        
            System.out.println("Server ready!");
        
        } catch (Exception e) {
            
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        
        }
        
    }
    
    

}
