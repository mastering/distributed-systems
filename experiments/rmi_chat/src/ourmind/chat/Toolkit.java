package ourmind.chat;


import java.util.Scanner;


public class Toolkit {
    
    public static class Constants {
    
        public static final String CHAT_SERVICE_NAME = "Chat";
        public static final String CHAT_SERVER_ADDRESS = "localhost";
        public static final int CHAT_SERVER_PORT = 23000;
        public static final int FAIL_CONECT_SERVER = 0x001;
        public static final int WAIT_TIME = 1000;
        public static final String NEW_LINE = "\n";
    
    }
    
    public static void handleException(Exception e) {
        System.err.println("Client exception: " + e.toString());
        e.printStackTrace();
    }
    
}

