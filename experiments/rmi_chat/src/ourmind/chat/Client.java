
package ourmind.chat;


import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class Client implements ChatClient {
    
    private ClientFrame clientFrame;
    private final String user;
    
    private Client(String user) {
        this.user = user;
    }
    
    @Override
    public void receive(Message message) {
        clientFrame.receive( message );
    }
    
    @Override
    public void login() {
        
        try {
            
            final Registry registry = LocateRegistry.getRegistry(
                Toolkit.Constants.CHAT_SERVER_ADDRESS, 
                Toolkit.Constants.CHAT_SERVER_PORT);
           
            // starting client registry
            final ChatClient stub = (ChatClient) UnicastRemoteObject.exportObject(this, 0);            
            registry.bind(user, stub);
            
            // connecting server registry
            ChatServer chatServer = (ChatServer) registry.lookup(Toolkit.Constants.CHAT_SERVICE_NAME);
            
            if (chatServer.login(user)) {
                clientFrame = new ClientFrame(chatServer, this, user);
            } else {
                throw new Exception("Cannot login on server :(");
            }
        
        } catch (Exception e) {
            Toolkit.handleException(e);
            System.exit(Toolkit.Constants.FAIL_CONECT_SERVER); 
        }
    
    }
    
    @Override
    public void logout() {
        
        try {
            
            LocateRegistry.getRegistry(
                Toolkit.Constants.CHAT_SERVER_ADDRESS, 
                Toolkit.Constants.CHAT_SERVER_PORT
            ).unbind(user);
        
        } catch (Exception e) {
            Toolkit.handleException(e);
        }
    
    }
    
    public static void main(String[] args) {
        
        for (String username : args) {
            Client client = new Client(username);
            client.login();
        }
        
        
    }
    
    private static class ClientFrame extends JFrame {
        
        private JTextArea messagesArea;
        private JTextField messageField;
        private final ChatServer chatServer;
        private final ChatClient chatClient;
        private final String userName;
        
        public ClientFrame(ChatServer chatServer, ChatClient chatClient, String userName) {
            
            super("Chat - " + userName);
            
            initComponents();
            initListeners();
            
            this.chatServer = chatServer;
            this.chatClient = chatClient;
            this.userName = userName;
            
        }
        
        
        public void receive(Message message) {
            String entry = message.getAuthor() + " says: " + message.getContent();
            messagesArea.append( entry + Toolkit.Constants.NEW_LINE );
        }
        
        private void send(String messageContent) {
            Message message = new Message(userName, messageContent);
            try { 
                chatServer.send(message); 
            } catch (Exception e) { 
                Toolkit.handleException(e); 
            }
        }
        
        private void onMessageFieldAction(ActionEvent e) {
            String message = messageField.getText();
            if (!message.isEmpty()) {
                send(message);
                messageField.setText("");
            }
        }
        
        private void onWindowClose(WindowEvent evt) {
            try {
                chatServer.logout(userName);
                chatClient.logout();
            } catch (Exception e) { 
                Toolkit.handleException(e); 
            }
        }
        
        private void initComponents() {
            
            setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
            
            Container pane = getContentPane();
           
            pane.setComponentOrientation( ComponentOrientation.RIGHT_TO_LEFT );
            pane.setLayout(new GridBagLayout());
            
            messageField = new JTextField(50);
            messageField.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent evt) {
                    onMessageFieldAction(evt);   
                }
                
            });
            
            messagesArea = new JTextArea(5, 50);
            messagesArea.setEditable(false);
            JScrollPane scrollPane = new JScrollPane(messagesArea);
            
            GridBagConstraints c = new GridBagConstraints();
            c.gridwidth = GridBagConstraints.REMAINDER;
            
            c.fill = GridBagConstraints.HORIZONTAL;
            pane.add(messageField, c);
            
            c.fill = GridBagConstraints.BOTH;
            c.weightx = 1.0;
            c.weighty = 1.0;
            pane.add(scrollPane, c);
            
            setSize(300, 600);
            //pack();
            setVisible(true);
        
        }
        
        private void initListeners() {
            
            addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent evt) {
                    onWindowClose(evt);
                }    
            });
            
        }
    }
    

}

