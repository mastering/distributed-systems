
package ourmind.chat;


import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


public interface ChatServer extends Remote {
    
    boolean login(String userName) throws RemoteException;
    boolean logout(String userName) throws RemoteException;
    void send(Message message) throws RemoteException;
    
}


