
import java.net.ServerSocket;
import java.net.Socket;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;

public class TCPClient {
    
    public static void main(String[] args) {
        
        Socket clientSocket = null;
        
        try {
            
            int serverPort = 6789;
            clientSocket = new Socket(args[1], serverPort);
            
            DataInputStream in = new DataInputStream( clientSocket.getInputStream() );
            DataOutputStream out = new DataOutputStream( clientSocket.getOutputStream() );
            
            out.writeUTF( args[0] );
            
            String reply = in.readUTF();
            System.out.println("Reply: " + reply);
        
        } catch (EOFException e) {
            System.out.println("EOFException: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
        } finally {
            
            try {
                if (clientSocket != null) clientSocket.close();
            } catch (IOException e) {
                System.out.println("IOException on close: " + e.getMessage());
            }
            
        }    
        
    }   
    
}
