import java.net.ServerSocket;
import java.net.Socket;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;

public class TCPServer {
    
    public static void main(String[] args) {
        
        try {
            
            int serverPort = 6789;
            ServerSocket serverSocket = new ServerSocket(serverPort);    
            while (true) {
                
                Socket clientSocket = serverSocket.accept();    
                new Connection( clientSocket );
                
            }
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
        }
        
    }

}

class Connection extends Thread {
    
    private DataInputStream in;
    private DataOutputStream out;
    private Socket clientSocket;
    
    public Connection(Socket clientSocket) {
        
        try {
            this.clientSocket = clientSocket;
            in = new DataInputStream( clientSocket.getInputStream() );
            out = new DataOutputStream( clientSocket.getOutputStream() );
            this.start();
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
        }
    
    }
    
    @Override
    public void run() {
        
        try {
        
            String data = in.readUTF();
            out.writeUTF(data);
        
        } catch (EOFException e) {
            System.out.println("EOFException: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
        } finally {
            
            try {
                clientSocket.close();
            } catch (IOException e) {
                System.out.println("IOException on close: " + e.getMessage());
            }
            
        }    
        
    }
    
}
