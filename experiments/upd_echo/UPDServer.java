
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.io.IOException;

public class UPDServer {
	
	public static void main(String[] args) {
		
		DatagramSocket serverSocket = null;

		try { 
			
			serverSocket = new DatagramSocket(6789);
			byte[] buffer = new byte[1000];
			while (true) {
				DatagramPacket request = new DatagramPacket(buffer, buffer.length);
				serverSocket.receive(request);
				DatagramPacket replay = new DatagramPacket(
				    request.getData(), 
				    request.getLength(), 
				    request.getAddress(), 
				    request.getPort()
			    );
				serverSocket.send(replay);
			}

		} catch (SocketException e) {
			System.out.println("Socket: " + e.getMessage());
		} catch (IOException e) { 
			System.out.println("IO: " + e.getMessage());
		} finally {
			if (serverSocket != null) serverSocket.close();
		}

	}

}
