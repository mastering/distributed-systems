
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.io.IOException;

public class UPDClient {
	
	public static void main(String[] args) {
		
		DatagramSocket aSocket = null;

		try { 
			
			aSocket = new DatagramSocket();
			byte[] data = args[0].getBytes();
			InetAddress serverAddress = InetAddress.getByName(args[1]);
			int serverPort = 6789;
			DatagramPacket request = new DatagramPacket(data, data.length, serverAddress, serverPort);
			aSocket.send(request);
			byte[] buffer = new byte[1000];
			DatagramPacket reply = new DatagramPacket(buffer, buffer.length);			
			aSocket.receive(reply);
			System.out.println("Reply: " + new String(reply.getData()));

		} catch (SocketException e) {
			System.out.println("Socket: " + e.getMessage());
		} catch (IOException e) { 
			System.out.println("IO: " + e.getMessage());
		} finally {
			if (aSocket != null) aSocket.close();
		}

	}

}
